def is_ascending(n: int) -> bool:
    s = str(n)
    return all(a < b for a, b in zip(s, s[1:]))


class Odometer:
    def __init__(self, size):
        self.size = size
        self.start = int("123456789"[:size])
        self.limit = int("123456789"[-size:])
        self.reading = self.start

    def __str__(self) -> str:
        return str(self.reading)

    def __repr__(self) -> str:
        return f'{self.start}<<<{self.reading}>>>{self.limit}'

    def forward(self, steps: int = 1):
        for _ in range(steps):
            if self.reading == self.limit:
                self.reading = self.start
            else:
                self.reading += 1
                while not is_ascending(self.reading):
                    self.reading += 1

    def backward(self, steps: int = 1):
        for _ in range(steps):
            if self.reading == self.start:
                self.reading = self.limit
            else:
                self.reading -= 1
                while not is_ascending(self.reading):
                    self.reading -= 1

    def distance(self, other) -> int:
        if self.size != other.size:
            return -1
        diff = 0

        self_copy = Odometer(self.size)
        self_copy.reading = self.reading

        while self_copy.reading != other.reading:
            diff += 1
            self_copy.forward()
        return diff
